#ifndef __UART_H__
#define __UART_H__

struct __uart_config__;
typedef struct __uart_config__ uart_config_t;

typedef enum {
    UART_CONFIG_115KBAUD_8DATAB_NOPARITY_NOFLOWCONTROL,
    UART_CONFIG_115KBAUD_8DATAB_ODDPARITY_NOFLOWCONTROL,
    UART_CONFIG_115KBAUD_8DATAB_EVENPARITY_NOFLOWCONTROL,
    UART_CONFIG_115KBAUD_8DATAB_NOPARITY_RTSCTS
} UART_CONFIG;

typedef enum {
    UART_PORTC,
    UART_PORTE
} UART_PORT;

typedef enum {
    DATABITS_5,
    DATABITS_6,
    DATABITS_7,
    DATABITS_8,
    DATABITS_9 = 7
} UART_DATABITS; 

typedef enum {
    PARITY_NONE,
    PARITY_EVEN = 2,
    PARITY_ODD
}UART_PARITY;

typedef enum {
    STOPBITS_1,
    STOPBITS_2
} UART_STOPBITS;

typedef enum {
    BAUD_9600,
    BAUD_115200,
    BAUD_921600
} UART_BAUD;

typedef enum {
    FLOWCONTROL_NONE,
    /*AVR Xmega USART doesnt have Dedicated RTS CTS pins
      But support can be provided by driving Port pins 
      to support RTS/CTS
      Though this implementation currently doesnt support it
    */
      
    FLOWCONTROL_RTSCTS,
} UART_FLOWCONTROL;

extern int init_uart(UART_CONFIG config, UART_PORT port);
extern int uart_xmit(UART_PORT port, uint8_t* data, unsigned int count);
extern int uart_recv(UART_PORT port, uint8_t* data, unsigned int count);
extern int numrxchars_available(UART_PORT port);
extern void flush_rxbuf(UART_PORT port);
#endif //__UART_H__
