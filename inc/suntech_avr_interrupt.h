#ifndef __SUNTECH_AVR_INTERRUPT_H__
#define __SUNTECH_AVR_INTERRUPT_H__

#define INTERRUPT(irq_handler)   \
    void irq_handler (void) __attribute__  ((signal, used, externally_visible)); \
    void irq_handler (void)


# define cli()  __asm__ __volatile__ ("cli" ::: "memory")
# define sei()  __asm__ __volatile__ ("sei" ::: "memory")
#endif //__SUNTECH_AVR_INTERRUPT_H__
