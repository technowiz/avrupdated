/*
 * suntech_lcd.h
 *
 */ 

#include "suntech_port.h"

#ifndef __LCD_H__
#define __LCD_H__

// Maximum number of common lines.
#define LCD_MAX_NR_OF_COM  4
// Maximum number of segment lines.
#define LCD_MAX_NBR_OF_SEG  40

// 7-segment with 4 commons terminals
#ifdef CONFIG_XMEGA_128B1_REVA
#  define FIRST_7SEG_4C 2
#else
#  define FIRST_7SEG_4C 10
#endif
#define WIDTH_7SEG_4C 5
#define DIR_7SEG_4C   1 // 1 = from right to left

// 14-segment with 4 commons terminals
#define FIRST_14SEG_4C 12
#define WIDTH_14SEG_4C 7
#define DIR_14SEG_4C   0 // 0 = from left to right

// Pixels With Blinking feature
#define ICON_LEVELBAR 0,0
#define ICON_USB      1,0
#define ICON_AVR      2,0
#define ICON_COLON    3,0
#define ICON_WARNING  0,1
#define ICON_IN       1,1
#define ICON_OUT      2,1
#define ICON_ERROR    3,1

 // Pixels With No-Blinking feature
#define ICON_AM         3,30
#define ICON_PM         0,29
#define ICON_DEGREE_C   3,11
#define ICON_DEGREE_F   2,11
#define ICON_VOLT       1,11
#define ICON_MILLI_VOLT 0,11

#define ICON_LEVEL_1    0,13
#define ICON_LEVEL_2    3,14
#define ICON_LEVEL_3    0,17
#define ICON_LEVEL_4    3,18
#define ICON_LEVEL_5    0,21
#define ICON_LEVEL_6    3,22
#define ICON_LEVEL_7    0,25
#define ICON_LEVEL_8    3,26

#define ICON_DOT_1      3,4
#define ICON_DOT_2      3,6
#define ICON_DOT_3      3,8
#define ICON_DOT_4      3,10

#define ICON_MINUS      0,10

// LCD Function Prototypes
void init_lcd(void);
void lcd_set_pixel(uint8_t pix_com, uint8_t pix_seg);

#endif /* __LCD_H__ */
