/**
 * \file
 *
 * \brief Management of C42048A LCD Glass component.
 *
 * Copyright (c) 2011-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include "c42048a.h"
#include "lcd.h"

void c42048a_init(void)
{
	// LCD_initialization
	lcd_clk_init();
	lcd_connection_init(PORT_MASK);
	lcd_timing_init(LCD_PRESC_16_gc, LCD_CLKDIV_DivBy4_gc,
			LCD_LP_WAVE_ENABLE_gc, LCD_DUTY);
	lcd_interrupt_init(LCD_INTLVL_OFF_gc, 16);
	lcd_blinkrate_init(LCD_BLINKRATE_2Hz_gc);
	lcd_enable();
}

void c42048a_bar_graph(uint8_t val)
{
	lcd_clear_pixel(ICON_LEVEL_1);
	lcd_clear_pixel(ICON_LEVEL_2);
	lcd_clear_pixel(ICON_LEVEL_3);
	lcd_clear_pixel(ICON_LEVEL_4);
	lcd_clear_pixel(ICON_LEVEL_5);
	lcd_clear_pixel(ICON_LEVEL_6);
	lcd_clear_pixel(ICON_LEVEL_7);
	lcd_clear_pixel(ICON_LEVEL_8);
	lcd_set_pixel(ICON_LEVELBAR);
	if (val>224) {
		lcd_set_pixel(ICON_LEVEL_8);
	}
	if (val>192) {
		lcd_set_pixel(ICON_LEVEL_7);
	}
	if (val>160) {
		lcd_set_pixel(ICON_LEVEL_6);
	}
	if (val>128) {
		lcd_set_pixel(ICON_LEVEL_5);
	}
	if (val>96) {
		lcd_set_pixel(ICON_LEVEL_4);
	}
	if (val>64) {
		lcd_set_pixel(ICON_LEVEL_3);
	}
	if (val>32) {
		lcd_set_pixel(ICON_LEVEL_2);
	}
	if (val>0) {
		lcd_set_pixel(ICON_LEVEL_1);
	}
}

/**
 * \brief Conversion of a half byte value in ASCII code
 *
 * \param val half byte to convert
 */
static uint8_t conv_ascii(uint8_t val)
{
	Assert(val<0x10);

	if (val<= 0x09) {
		val = val + 0x30;
	}
	if ((val >= 0x0A) && (val<= 0x0F)) {
		val = val + 0x37;
	}
	return val;
}

