#include <avr/io.h>
#include "suntech_avr_interrupt.h"
#include "suntech_port.h"
#include "suntech_rtc.h"

volatile static unsigned int rtccount;
volatile static unsigned int delay_countdown_timer;
INTERRUPT(__vector_RTC_OVF_INT)
{
    rtccount++;
    if(delay_countdown_timer > 0) {
        delay_countdown_timer--;
    }
}

void rtc_delay(unsigned int delay) {
    cli();
    delay_countdown_timer = delay;
    sei();
    while(delay_countdown_timer > 0) {
    };
}

void init_rtc(void)
{
    rtccount = 0;
    PR.PRGEN &= ~0x4;
    /*  A period of 328 will give us a 10 ms overflow interrupt*/
    RTC.PER =  328;
    /* set the overflow interrupt level to medium*/
    RTC.INTCTRL = 0x2;
    /*Dont divide the RTC Clock. Let it be 32768 Hz and enable the clock*/
    RTC.CTRL = 1;
}
