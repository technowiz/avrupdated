/*
 * General Description:
 * 1. Configure System Clocks.  Choose XTAL 8 MHz osc for PLL Src
 * 2. Choose PLL as system clock
 * 3. PLL OUT = 16 MHz
 * 4. CLKSYS = 16 MHz
 * 5. PRESCALER A DIV by 2 and so CLKPER4 = 8 MHz
 * 6. PRESCAKER B Div by 1 CLKPER2 = 8 MHz
 * 7. PRESCALER C Div by 1 CLKPER = 8 MHz
 * Note: UART Baud rates are less jitter with higher per clock
 */ 

// Required AVR Libraries
#include <avr/io.h>
#include "suntech_avr_interrupt.h"
#include "suntech_timer.h"
#include "suntech_uart.h"
#include "suntech_port.h"
#include "suntech_adc.h"
#include "suntech_rtc.h"
#include "c42048a.h"

// User-Created Include Files

// Function Prototypes
void clock_setup_pllxosc_usb(void);
void clock_setup_32khz(void);
void pmic_init(void);
void board_init(void);
static int portopen;
/////////////////////////////////////////////////////////////////////
// The main function follows a work flow that has been standardized
// within the ASF for Xmega devices. This means that when searching
// through Xmega example projects in ASF, you will find a main
// function with the same general flow:
// 
// clock_setup();
// pmic_init();
// board_init();
// -- peripheral initiallization --
// while(1) {
//     -- application code --
// }
//
/////////////////////////////////////////////////////////////////////
int main(void)
{
	// LCD Test Strings
	uint8_t lcd_test_alpha_string[] = "XMEGA";
	
    // Configure System and Peripheral Clocks
    PR.PRGEN = 0xFF;
    PR.PRPA = 0xFF;
    PR.PRPB = 0xFF;
    PR.PRPC = 0xFF;
    PR.PRPE = 0xFF;
    /*32 KHz RC OSC internal used as source for RTC
    *RTC for PORT Input debouncing
    */
    clock_setup_32khz();
    clock_setup_pllxosc_usb();
	
	// Configure Interrupt Priority Level and Location of Vectors
	pmic_init();
	
	// Configure Board I/O
	
	// Initialize the LCD Controller
	
	// Enable interrupts
	sei();
	init_port();
    //power up both TC0 and TC1. Going to use them to generate PWMs for the LEDs
    PR.PRPC &= ~3;
    //enable USARTE in PR.PRPE Bit 4
    PR.PRPE &= ~0x10;
	
    init_timer(PWM_LED0);
    init_timer(PWM_LED1);
    TCC0.INTFLAGS |= 0x1;
    TCC1.INTFLAGS |= 0x1;
    portopen = -1;
    portopen = init_uart(UART_CONFIG_115KBAUD_8DATAB_NOPARITY_NOFLOWCONTROL, UART_PORTE);
    init_adc();
    init_rtc();
    start_conversion();
	
    c42048a_init();
    c42048a_bar_graph(225);
    c42048a_write_alpha_packet("Suntech");
	while(1) {
		// Infinite Loop to add code and allow LCD
		// Frame Interrupt to continuously operate
        if(portopen == 0) {
            rtc_delay(1);
            if((PORTA.IN & 0x40) == 0x00) {
                uart_xmit(UART_PORTE,(uint8_t*) "Suntech ", 8);
            }
            if((PORTA.IN & 0x80) == 0x80) {
                uart_xmit(UART_PORTE,(uint8_t*) "Medical\n", 8);
            }
            check_port_inputs();
        }
	}	
}

void clock_setup_pllxosc_usb(void) {
    OSC.XOSCCTRL = 0x43;
	CCP = 0xd8;
    OSC.CTRL |= OSC_XOSCEN_bm;
	while (0 == (OSC.STATUS & OSC_XOSCRDY_bm)); // wait for STATUS reg to read 0x08
	CCP = 0xd8;
    //Choose XOSC as PLL source and multiply XOSC by 6 to get a 48 MHz system clock
    //Dont divide PLL Output
    // This will also be fed to the USB
    OSC.PLLCTRL = (0x3 << 6) | (0x6);
    OSC.CTRL |= OSC_PLLEN_bm;
	while (0 == (OSC.STATUS & OSC_PLLRDY_bm)); // wait for STATUS reg to read 0x08
	CCP = 0xd8;
    /*Prescaler A div by 2. So CLKPER4 = 24 MHz
    * Prescaler B divide by 1 and Prescaler C divide by 2
    * CLKPER2 = 24 MHz and CLKCPU = CLKPER = 12 MHz
    */
	CLK.PSCTRL = (1 << 2) | (0x1); 
	CCP = 0xd8;
    /*Enable CLKSYS on PLL at 48 MHz*/
	CLK.CTRL = 0x4; // 0x04
}

void clock_setup_32khz(void) {
    //OSC.XOSCCTRL = 0x43;
    //OSC.CTRL |= OSC_XOSCEN_bm;
	//while (0 == (OSC.STATUS & OSC_XOSCRDY_bm)); // wait for STATUS reg to read 0x08
    OSC.CTRL |= OSC_RC32KEN_bm;
	while (0 == (OSC.STATUS & OSC_RC32KRDY_bm)); // wait for STATUS reg to read 0x08
	 
	//OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | 0x1; // 0x03
	CCP = 0xd8;
	CLK.PSCTRL = 0; // 0x04
	CCP = 0xd8;
	CLK.CTRL = 0x2; // 0x04
	CCP = 0xd8;
    /*Enabe RTC on internal RC32 KHz oscillator PORT inputs. PORT IO Debouncing*/
	CLK.RTCCTRL = 0xD; 
}

void pmic_init(void) {
	
	//////////////////////////////////////////////////////////////////////
	//PMIC.CTRL
	//     7       6       5       4       3        2         1         0
	// | RREN  | IVSEL |   -   |   -   |   -   | HILVLEN | MEDLVLEN | LOLVLEN |
	//     0       0       0       0       0        0         0         0
	// Enable all interrupt levels
	// Load Int vectors to application section of flash
	// Set interrupt priority to Static (lower address = higher priority)
	PMIC.CTRL = PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm | 0x80; //0x07
	//////////////////////////////////////////////////////////////////////
	
}

void device_abort(void)
{
    while(1)
    {
        PORTB.OUTTGL = 0x80;
    }
}

