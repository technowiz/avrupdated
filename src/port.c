#include <avr/io.h>
#include "suntech_avr_interrupt.h"
#include "suntech_port.h"
#include "suntech_timer.h"
#include "suntech_adc.h"
#include "suntech_rtc.h"

#define PA0_PA1_CONNECTED  0x1
#define PA0_PA3_CONNECTED  0x2
#define PA5_PA3_CONNECTED  0x4

#define PA_PWM_RUNNING_MASK  (1 << 1)
static unsigned char pa_state;
static void check_port0_int(void)
{
    if((PORTA.IN & 0x1) == 0x1) {
    /* we have PA0 asserted
    * Determine the source
    * 1. Turn Off PA1
    * 2. Check if PA0 goes to Zero if it does PA1 is the driver 
    * 3. else Turn Off PA3
    * 4. Check if PA0 goes to Zero if it does PA3 is the driver 
    */
        if((pa_state & 0x3) != 0) {
            return;
        }
        PORTA.OUT &= ~2;
        rtc_delay(5);
        if((PORTA.IN & 0x1) == 0x0) {
            //set LED0 at 50% duty cycle and 1 Hz
            stop_pwm(PWM_LED0);
            set_period(PWM_LED0, 46875);
            set_duty_cycle(PWM_LED0, 50);
            start_pwm(PWM_LED0);
            pa_state |= PA0_PA1_CONNECTED;
        } else {
        /* Do similar but this time with PA3*/
            PORTA.OUT &= ~8;
            rtc_delay(5);
            if((PORTA.IN & 0x1) == 0x0) {
            //set LED0 at 75% duty cycle and 1 Hz
                stop_pwm(PWM_LED0);
                set_period(PWM_LED0, 23437);
                set_duty_cycle(PWM_LED0, 75);
                start_pwm(PWM_LED0);
                pa_state |= PA0_PA3_CONNECTED;
            } else {
                stop_pwm(PWM_LED0);
                PORTB.OUT |= 0x10;
                pa_state &= ~3;
            }
            PORTA.OUT |= 0x8;
        }
        /* PA0 connected to PA1
        *  Turn back ON PA1 
        */
        PORTA.OUT |= 0x2;
    } else {
    /*Turn off LED0*/
        stop_pwm(PWM_LED0);
        PORTB.OUT |= 0x10;
        pa_state &= ~3;
    }
}

static void check_port1_int(void)
{
    unsigned int  vout = get_pot_vout();
    unsigned int period = 468;
    if((PORTA.IN & 0x20) == 0x20) {
        if(pa_state & PA5_PA3_CONNECTED) {
            return;
        }
    /* we have PA5 asserted
    * Determine the source
    * 1. Turn Off PA3
    * 2. Check if PA5 goes to Zero if it does PA3 is the driver 
    * 3. else  stop LED1 and the PWM to it
    */
        PORTA.OUT &= ~8;
        rtc_delay(5);
        if((PORTA.IN & 0x20) == 0x0) {
             //set LED1 at 50% duty cycle and 2 Hz
             stop_pwm(PWM_LED1);
             vout = get_pot_vout();
             if(vout < 0x10) vout = 0x10;
             else if(vout > 0x74) vout = 0x73;
             period = period * (0x74 - vout) ;
             set_period(PWM_LED1, period);
             set_duty_cycle(PWM_LED1, 50);
             start_pwm(PWM_LED1);
             pa_state |= PA5_PA3_CONNECTED;
        } else {
             pa_state &= ~PA5_PA3_CONNECTED;
             stop_pwm(PWM_LED1);
             PORTB.OUT |= 0x20;
        }
        /* PA5 connected to PA3
        *  Turn back ON PA3 
        */
        PORTA.OUT |= 0x8;
    } else {
         pa_state &= ~PA5_PA3_CONNECTED;
         stop_pwm(PWM_LED1);
         PORTB.OUT |= 0x20;
    }
}

void check_port_inputs(void)
{
    check_port0_int();
    check_port1_int();
}
#if 0
/*Abandoned the interrupt handlers on inputs due to debouncing issues*/
INTERRUPT(__vector_PORTA_INT0)
{
    cli();
    //PORTB.OUTTGL = 0x80;
    check_port0_int();
    PORTA.INTFLAGS |= 0x1;
    sei();
}

INTERRUPT(__vector_PORTA_INT1)
{
    cli();
    //PORTB.OUTTGL = 0x40;
    check_port1_int();
    PORTA.INTFLAGS |= 0x2;
    sei();
}
#endif

void init_port(void) {
	
    pa_state = 0;
	// LED Pins set to Outputs
	// Sensor Pins set to Inputs
	PORTB.DIR = 0xF0;
	PORTB.OUTSET = 0xF0; //4 LEDs Off
    /*Set PORTC pins 0, 6 and 7 to outputs
    * PC7 is CLKPER4
    * PC6 is RTC
    */
    PORTC.DIR = 0xC3;
    //Output CLKPER on PC7. Useful to scope and measure the outputs
    PORTCFG.CLKEVOUT =  (1 << 6) | 0x1;
	
    /*
    * 1. Set PA0 and PA5 as inputs.
    * 2. Enable PA0 interrupt both rising and falling edge on PORTA INT0
    * 3. Enable PA5 interrupt both rising and falling edge on PORTA INT1
    * 4. Set PA1 and PA3 as outputs
    * 5. When INT0 is triggered if PA0 = 0 stop LED0 PWM
    * 6. When INT0 is triggered if PA0 = 1 then toggle PA1 and PA3
    *    to determine the source of PA0 going high.
    *    If PA1 is the driver then PWM LED0 should go at 1 Hz 50% duty
    *    If PA3 is the driver then PWM LED0 should go at 2 Hz 75% duty
    * 7. When INT1 is triggered if PA5 = 0 Stop LED1 PWM
    * 8. When INT1 is triggered if PA5 = 1 then toggle PA3
    *    If PA3 is the driver then enable LED1 PWM
    */
    PORTA.DIR = 0xA;

    /* ON the input pins dont configure any internal pull ups or pull downs.
    * looks like the output drivers on io pins are not strong to drive them high
    * to overcome
    */
    PORTA.PIN0CTRL = 0;
    PORTA.PIN1CTRL = 0;
    PORTA.PIN3CTRL = 0;
    PORTA.PIN5CTRL = 0;
    /* Pull up PA6 to VCC so we can detect when it is grounded exernally
    */
    PORTA.PIN6CTRL = 0x3 << 3;
    /* Pull down on PA7 so we can detect when it is driven high by VCC*/
    PORTA.PIN7CTRL = 0x2 << 3;
    //drive PA1 and PA3 high and all others low just to take care of the spurious high when the DIR bits are written
    PORTA.OUT = 0xA;
    //clear any spurious port interrupt flags on init
    PORTA.INTFLAGS = 0x3;
    //enable portA INT0 interrupt on PA0
    PORTA.INT0MASK = 0x1;
    //enable PORTA INT1 interrupt on PA5
    PORTA.INT1MASK = 0x20;
    /*DONT enable INT0 and INT1 on PORTA 
    * Problems with Port interrupts
    * Using RTC to debounce the inputs every 10 ms
    * Timer TC0 and TC1 interrupts are medium level
    */
    PORTA.INTCTRL = 0x0;
}
