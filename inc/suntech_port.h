/*
 * suntech_port.h
 *
 */ 


#ifndef __SUNTECH_PORT_H__
#define __SUNTECH_PORT_H__

// Project Definitions
#define LED3 0x80;
#define LED2 0x40;
#define LED1 0x20;
#define LED0 0x10;
#define BL_PIN 0x20;
#define CONFIG_XMEGA_128B1_REVA //Comment this out if using a later revision
#define CONFIGURATION_CHANGE_PROTECTION  do { CCP = 0xd8; } while (0)

void init_port(void);
void check_port_inputs(void);
	
#endif /* __SUNTECH_PORT_H__*/
