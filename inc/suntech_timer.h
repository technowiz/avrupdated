#ifndef __AVR_PWMTIMER_H__
#define __AVR_PWMTIMER_H__


typedef enum {
    PWM_LED0,
    PWM_LED1
}PWM_INDEX;
void init_timer(PWM_INDEX ind);
void start_pwm(PWM_INDEX ind);
void stop_pwm(PWM_INDEX ind);
void set_duty_cycle(PWM_INDEX ind, unsigned int dutypercent);
void set_period(PWM_INDEX ind, unsigned int period);
#endif //__AVR_PWMTIMER_H__
