#include <avr/io.h>
#include "suntech_avr_interrupt.h"
#include "suntech_port.h"
#include "suntech_timer.h"
/* In Free running if the interrupt is enabled the ADC
* interrrupt handler is going to swamp the ALU
* Instead let the ADC run in Free running mode
* but disable the interrupt. 
* When LED1 is required to run then read ADCB.CHORES
* and use it to scale the period
* which is constantly updated because of the free running
* ADCB
*/
INTERRUPT(__vector_ADCB_CH0INT)
{
    cli();
    /*conversion complete interrupt
    * read the result 
    * This interrupt handler is only used to check that 0x80
    * represents half of the bandgap voltage which is 0r.x5V
    * On the board used for testing the pot was  close 22K not 25K
    * and so the max voltage at PB1 seen is 0.725 volts
    * so looks like there may be a variability in the pots across
    * each board
    * to keep it simple we will use only 0 to 0.6 V which to 
    * represent the 1 Hz thru 100 Hz freq
    * This scales it nicely with each unit of the ADC REsult 
    * mapping to 1 Hz
    */
    PORTB.OUTTGL = 0x40;
    #if 0
    if((result) > 0x00A0) {
        PORTB.OUT |= 0x40;
        PORTB.OUT &= ~0x80;
    }else {
        PORTB.OUT |= 0x80;
        PORTB.OUT &= ~0x40;
    }
    #endif
    ADCB.INTFLAGS = 1;
    sei();
}
/*
* 1. PB1 on ADCB1 is the input driven by the Potentiometer
  2. Potentionmeter is a 3266P-1-253_LF which is a 25K POT
     The VIN to the POT is at 3.3 V
     Vout to Vin has a minimum of 100K resistor in between (pull up)
     so the max Vout  = 3.3  * (25/(100+25)) = 0.66 V
     So the full range of Vout is 0 to 0.66 V
     with Single ended positive input  
          8 bit resolution 
          Vref = VTG/5 = 1V 
          the expected range of ADC result is 0x00 to 0xA8
          the LED1 frequency of 1Hz to 100 Hz needs to be mapped to a full range of 0xA8
*/
void init_adc(void)
{
    /*Turn on Clock to ADC on Port B*/
    PR.PRPB &= ~0x2;
    /*CLKPER is the source to ADCB
    * CLKPER is 8 MHz. See clock_setup_pllxosc in main.c
    * If we run the ADC at 1/8 th of the CLKPER Speed
    * the ADC clock unit will be hopefully long enough for a better 
    * conversion at higher resistance values of the potentiometer
    * Select CLPPER/8 for adc prescaler
    */
    /*Enable the ADCB*/
    ADCB.CTRLA = 0x1;
    ADCB.PRESCALER = 0x3;
    /*ADC REf clock will be VTG/5 = 1V*/
    ADCB.REFCTRL = 0x2;
    /*CTRLB of ADCB choose 
      - 8 bit resolution
      - Freerunning so the conversion goes continuously
      - with unsigned conversion mode
      - and no current limit. current consumption is not a concern right now
      */
    ADCB.CTRLB = 0x0C;
    /*to start with set SAMPCTRL to 0xF which gives 16 ADC lf clock cyclesi
     of sampling time
     Depending on the performance of the result either the prescaler can be increased
     or this register can be set to a higher rate to sample the vout accurately
     */
    ADCB.SAMPCTRL = 0xF;
    /*Now set the ADCB CH0 to sample the input on ADCB1*/
    /*CH CTRL 
     - Single Ended Input mode
     - No gain
    */
    ADCB.CTRLA |= 0x04;
    /*ADC Mux control register
      - MUXPOS on ADCB Pin 1
      -  FOR Single Ended MUXNEG is not used
    */
    ADCB.CH0.CTRL = (0x1);
    ADCB.CH0.MUXCTRL = (0x1 << 3);
    /*SCAN is zero since only ADCB1 is sampled*/
    ADCB.CH0.SCAN = 0;
    /*ADC interrupt on conversion complete with interrupt level low
    * we dont need ADC interrupt on at all
    * when PORTA INT1 is triggered call get_pot_vout
    * which will return the current value of the ADCB PB1
    * The ADCB block is running free and continuously converting the only pin PB1
    * which is good enough
    */
    //ADCB.CH0.INTCTRL = 0x01;
}

void start_conversion(void)
{
    ADCB.CH0.CTRL |= 0x80;
    /*Alternately the following does the same*/
    //ADCB.CTRLA |= 0x4;
}

void stop_conversion(void)
{
    ADCB.CH0.CTRL &= ~0x80;
    /*Alternately the following does the same*/
    //ADCB.CTRLA &= ~0x4;
}

volatile unsigned int get_pot_vout(void)
{
    volatile unsigned int vout = (unsigned int)ADCB.CH0RES;
    return vout;
}
