/*
 * suntech_adc.h
 *
 */ 


#ifndef __SUNTECH_ADC_H__
#define __SUNTECH_ADC_H__
// Project Definitions


void init_adc(void);
void start_conversion(void);
void stop_conversion(void);
volatile unsigned int get_pot_vout(void);

#endif //__SUNTECH_ADC_H__
