#include <avr/io.h>
#include "suntech_avr_interrupt.h"
#include "suntech_uart.h"

#ifdef  CONFIG_UART_RXBUFSIZE
#define MAX_RXBUFSIZE  CONFIG_UART_RXBUFSIZE
#else
#define MAX_RXBUFSIZE  (1 << 7) //128 bytes of rx buffer
#endif

struct __uart_config__ {
    UART_BAUD baudrate;
    UART_DATABITS databits;
    UART_PARITY parity;
    UART_STOPBITS stopbits;
    UART_FLOWCONTROL flowcontrol;
};

static uart_config_t config_115kbaud_8n1 = {
    .baudrate = BAUD_115200,
    .databits = DATABITS_8,
    .parity = PARITY_NONE,
    .stopbits = STOPBITS_1,
    .flowcontrol = FLOWCONTROL_NONE
};

static unsigned char rxbuffer[MAX_RXBUFSIZE];
static unsigned int pwrite;
static unsigned int pread;

int baudreg_vals_12M_Clk [] = {
    -3, 617,
    -7, 705,
    -4, -3    
};

int baudreg_vals_8M_Clk [] = {
    -6, 3269,
    -7, 428,
    -7, -59    
};

int baudreg_vals_4M_Clk [] = {
    -7, 3205,
    -6, 75,
    -7, -93
};

int baudreg_vals_2M_Clk [] = {
    0, 12,
   -7, 11, 
    -7, -111
};

/*
* static functions
*/

static void set_baud(USART_t* port, UART_BAUD baud) {
    /*Using internal clock for baud generation clkper is at 8 MHz*/
    int bscale;
    int bsel;
    unsigned int baudindex = baud << 1;
    if(port == 0) return;
    bscale = baudreg_vals_12M_Clk[baudindex];
    bsel = baudreg_vals_12M_Clk[baudindex + 1];
    port->BAUDCTRLA = bsel & 0xFF;
    port->BAUDCTRLB = (bscale << 4) | (((bsel >> 8) & 0xF));
}

static void set_parity(USART_t* port, UART_PARITY parity) {
    if(port == 0) return;
     
    port->CTRLC &= ~(0x30);
    if(parity == PARITY_EVEN) {
        port->CTRLC |= 0x20;
    } else if(parity == PARITY_ODD) {
        port->CTRLC |= 0x30;
    }
}

static void set_databits(USART_t* port, UART_DATABITS dbits) {
    if(port == 0) return;
    port->CTRLC &= ~7;
    port->CTRLC |= (dbits & 7);
}

static void set_stopbits(USART_t* port, uint8_t stopbits) {
    if(port == 0) return;
    port->CTRLC &= ~8;
    port->CTRLC |= ((stopbits & 1) << 3);
}

void flush_rxbuf(UART_PORT port)
{
    pread = 0;
    pwrite = 0;
}


INTERRUPT(__vector_USARTE0_RXC_INT)
{
    rxbuffer[pread++] = USARTE0.DATA;
    if(pread == MAX_RXBUFSIZE) {
        pread = 0;
    }
    USARTE0.STATUS |= 0x80;
}

int init_uart(UART_CONFIG config, UART_PORT portind)
{
    uart_config_t* pconfig = &config_115kbaud_8n1;
    USART_t* port = 0;
    if(portind == UART_PORTE) {
        port = (USART_t *) 0x0AA0;
        PORTE.DIR &= ~0xC;
        /*set  TX0 as output*/
        PORTE.DIR |= 0x8;
        PORTE.OUT &= ~0xC;
    } else if(portind == UART_PORTC) {
        port = (USART_t *) 0x08A0;
        PORTC.DIR &= ~0xC;
        /*set TX0 as output*/
        PORTC.DIR |= 0x8;
        PORTC.OUT &= ~0x8;
    } else {
        //no port set return.
        return -1;
    }
    if(config != UART_CONFIG_115KBAUD_8DATAB_NOPARITY_NOFLOWCONTROL) {
        //currently only this config is supported
        return -2;
    }
    /*Disable port interrupts first*/
    port->CTRLA = 0;
    port->CTRLB = 0;
    port->CTRLC = 0;
    /*enable both transmitter and receiver on this port*/
    /*set baud and other parameters*/
    set_baud(port, pconfig->baudrate);
    set_parity(port, pconfig->parity);
    set_databits(port, pconfig->databits);
    set_stopbits(port, pconfig->stopbits);
    port->CTRLB |= 0x3 << 3;
    port->STATUS = 0;
    port->CTRLA |= (0x3 << 4);
    
    /*Configure the Rx circular buffer for the ISR*/
    flush_rxbuf(portind);
    return 0;
}

int uart_xmit(UART_PORT portind, uint8_t* data, unsigned int total)
{
    int count = total;
    volatile USART_t* port = 0;
    if(portind == UART_PORTE) {
        port = (USART_t *) 0x0AA0;
    } else if(portind == UART_PORTC) {
        port = (USART_t *) 0x08A0;
    }
    if(port == 0) return 0;
    while(count > 0) {
        /* make sure Data buffer is empty */
        //while(!(port->STATUS & (1 << 5)));
        while(!(port->STATUS & (1 << 5)));
        port->DATA = *(data++);
        while(!(port->STATUS & (1 << 6)));
        //while(!(port->STATUS & (1 << 6)));
        /*make sure data in shift register is pushed to the wire */
        //while(!(port->STATUS & (1 << 6)));
        /*make sure data buffer is empty
        * the loop below should automatically end with out looping
        * as the DAta buffer flag will be set right after the Transmit shift register
        *. If either of these loops are stuck we will be in trouble
        * ideally should be handled in a interrupt without having to poll these bits
        * But that would then necessitate a mechanism to timed block this call
        * too much to handle for now
        */
        while(!(port->STATUS & (1 << 5)));
        count--;
    };
    return total;
}

int uart_recv(UART_PORT port, uint8_t* data, unsigned int count)
{
    int total = 0;
    if(count == 0) return 0;
    while(pread != pwrite) {
        *data = rxbuffer[pread++];
        data++;
        if(pread == MAX_RXBUFSIZE) {
            pread = 0;
        }
        total++;
        if(total == count) break;
    }
    return total;
}

int numrxchars_available(UART_PORT port)
{
    /* We have only one circular buffer. So if the caller changes the UART on us
     he/she should flush the buffer. 
     Now one thing is not clear. Since the base address of the UART on PORTC is different
     than the one on PORTE are there two different digital blocks of UART implementations
     that can simultaneously be instantiated or is it just one digital block of UART with 
     two different base addresses so that they are mapped to two different port pins.
     Not clear. One way is to try and instantiate two UARTs and write to both of them at the same
     time and see if they are two different or the same UART.
     Anyway we will only support one buffer. so up to the user that uses this interface to take care
     of not instantiating two UARTs or implement two dedicated buffers one for each PORT
     Right now we dont care of the argument that is passed in.  It is only there if we intend to extend
     the implementation
    */
    int count = 0;
    if(pread == pwrite) return 0;
    if(pwrite > pread) {
        count = pwrite - pread;
    } else {
        count = MAX_RXBUFSIZE - pread;
        count += pwrite;
    }
    return count;
}
