#include <avr/io.h>
//#include <avr/interrupt.h>
#include "suntech_avr_interrupt.h"
#include "suntech_port.h"
#include "suntech_timer.h"

INTERRUPT(__vector_TCC1_CCA_INT)
{
	// Off LED1
	PORTB.OUT |= LED1;
    TCC1.INTFLAGS |= 0x10;
}

INTERRUPT(__vector_TCC0_CCA_INT)
{
	// Off LED0
	PORTB.OUT |= LED0;
    TCC0.INTFLAGS |= 0x10;
}

INTERRUPT(__vector_TCC0_OVF_INT)
{
	// ON LED0
	PORTB.OUT &= ~LED0;
    TCC0.INTFLAGS |= 0x1;
}

INTERRUPT(__vector_TCC1_OVF_INT)
{
	// ON LED1
	PORTB.OUT &= ~LED1;
    TCC1.INTFLAGS |= 0x1;
}

void init_timer(PWM_INDEX ind)
{
    TC0_t* pTimer = (TC0_t*) 0x0800;

    if(ind == PWM_LED1) {
        pTimer++;
    }
    //clkper = 12 MHz
    //set prescaler to divide by 256
    //Therefore timer clock after prescaler = 46875 Hz
    // To get 1 HZ the period has to be = 46875
    pTimer->CTRLA = 6;
    //set wgmode as single slope pwm
    pTimer->CTRLB = 0x3;
    //clkper is at 12MHz and Prescaler is set Div by 4 we have 125KHz source clk
    //With a period of 125 the frequency of the PWM will be 1000 Hz
    pTimer->PER =  46875; /*Default 1 Hz frequency of timer*/
    //set PWM duty cycle at 50%
    pTimer->CCABUF = 23437;
}

void start_pwm(PWM_INDEX ind)
{
    //enable TC0 Channel A compare
    if(ind > PWM_LED1 || ind < PWM_LED0) return;
    if(ind == PWM_LED0) {
    /*LED0 is driven by Timer 0 PWM indirectly thru interrupt*/
        TCC0.CTRLB |= 0x10;
        TCC0.INTCTRLA = 0x2;
        TCC0.INTCTRLB = 0x2;
    }
    else if(ind == PWM_LED1) {
    /*LED1 is driven by Timer 1 PWM indirectly thru interrupt*/
        TCC1.CTRLB |= 0x10;
        TCC1.INTCTRLA = 0x2;
        TCC1.INTCTRLB = 0x2;
    }
}

void stop_pwm(PWM_INDEX ind)
{
    //enable TC0 Channel A compare
    if(ind > PWM_LED1 || ind < PWM_LED0) return;
    if(ind == PWM_LED0) {
        TCC0.CTRLB &= ~0x10;
        TCC0.INTCTRLA &= ~0x2;
        TCC0.INTCTRLB &= ~0x2;
    }
    else if(ind == PWM_LED1) {
        TCC1.CTRLB &= ~0x10;
        TCC1.INTCTRLA &= ~0x2;
        TCC1.INTCTRLB &= ~0x2;
    }
}

void set_period(PWM_INDEX ind, unsigned int period)
{
    if(ind == PWM_LED0) {
        TCC0.PER = period ;
    } else if(ind == PWM_LED1) {
        TCC1.PER = period ;
    }
}

static unsigned int divide(unsigned int dividend, unsigned int divisor)
{
    unsigned int quotient = 0;
    unsigned int remainder;
    remainder = dividend;
    while(remainder >= divisor) {
        remainder -= divisor;
        quotient++;
    }
    return quotient;
}

void set_duty_cycle(PWM_INDEX ind, unsigned int percent)
{
    if(ind == PWM_LED0) {
        TCC0.CCABUF = divide(TCC0.PER, 100) * percent ;
    } else if(ind == PWM_LED1) {
        TCC1.CCABUF = divide(TCC1.PER, 100) * percent ;
    }
}
